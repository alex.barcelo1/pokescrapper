If you are using this on windows you must modifie the line 196
driver = webdriver.Chrome(executable_path=r'XXXXXXXX',options=opt)
Replace XXXXXXXX by the path to chromedriver.exe
(Probably something like 'C:\Users\YOUR_USERNAME\AppData\Local\Programs\Python\Python38-32\Lib\site-packages\chromedriver\chromedriver.exe')


This program allows you to see quickly relevant informations concerning your opponent's team on pokemon showdown
Usage : python ./pokescrap.py [-v] url
    -v : OPTIONAL, outputs data about every pokemon on both teams such as stats, speedtiers and usual set
    url : A url to a showdown 1v1 match (ex:https://play.pokemonshowdown.com/battle-gen8ou-1140114026)


Build the dockerfile:
    requirements : having Docker setup on your computer 
    (https://docs.docker.com/docker-for-windows/install/
    https://docs.docker.com/docker-for-mac/install/
    https://docs.docker.com/engine/install/ubuntu/)

    (sudo) docker build -t pokescrap_containered .
    (sudo) docker run pokescrap_containered [-v] url

Future features:
    Scouting your opponent's last games for his sets
    Relevant damage calcs
    CLI damage calcs