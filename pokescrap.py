from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import re, sys

tot_weak = {
    "Bug": 0,
    "Dark": 0,
    "Dragon": 0,
    "Electric": 0,
    "Fairy": 0,
    "Fighting": 0,
    "Fire": 0,
    "Flying": 0,
    "Ghost": 0,
    "Grass": 0,
    "Ground": 0,
    "Ice": 0,
    "Normal": 0,
    "Poison": 0,
    "Psychic": 0,
    "Rock": 0,
    "Steel": 0,
    "Water": 0
}

checks = {
    "Bug": 0,
    "Dark": 0,
    "Dragon": 0,
    "Electric": 0,
    "Fairy": 0,
    "Fighting": 0,
    "Fire": 0,
    "Flying": 0,
    "Ghost": 0,
    "Grass": 0,
    "Ground": 0,
    "Ice": 0,
    "Normal": 0,
    "Poison": 0,
    "Psychic": 0,
    "Rock": 0,
    "Steel": 0,
    "Water": 0
}

ab_imm = {
    "Dry Skin": "Water",
    "Flash Fire": "Fire",
    "Sap Sipper": "Grass",
    "Levitate": "Ground",
    "Lightningrod": "Electric",
    "Motor Drive": "Electric",
    "Storm Drain": "Water",
    "Volt Absorb": "Electric",
    "Water Absorb": "Water"
}

sr_res=0

silent=1

def print_weak_dic(d):
    global sr_res
    for k,v in d.items():
        if not silent:
            print(k,v)
        if "eak to" in k:
            for types in v:
                tot_weak[types]+=1
        if "esists" in k or "mmune" in k:
            for types in v:
                checks[types]+=1
                if types == "Rock":
                    sr_res+=1

def reset_dicts():
    for key in tot_weak.keys():
        tot_weak[key]=0
    for key in checks.keys():
        checks[key]=0
    global sr_res
    sr_res=0

def print_team_weak():
    sorted_tot_weak = {k: v for k, v in sorted(tot_weak.items(), key=lambda item: item[1])}
    for k,v in sorted_tot_weak.items():
        if v > 1:
            print(v," of this team's pokemon are weak to",k)

    for k,v in checks.items():
        if v==0:
            print("No checks to",k)

    print (sr_res,"stealth rock resistant pokemons")

def print_weakness(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    soup = soup.find('dl', {'class':'TypeEffectives'})
    keys = soup.find_all('dt')
    vals = soup.find_all('ul')
    #Strongly resists:
    #FireWater
    r = {}
    for i in range(len(keys)):
        key = keys[i].text[:-1]
        val=vals[i].text
        r[key]=val

    for key in r.keys():
        r[key] = re.findall('[A-Z][^A-Z]*',r[key])
    
    print_weak_dic(r)

def print_usual_set(driver,pkmn):
    try :
        driver.find_element_by_class_name('ExportButton').click()
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        soup = soup.find('div', {'class':'BlockMovesetInfo'})
        if (not silent):
            print(soup.text)
        ability = re.search(r"[\s\S]*Ability: (.*)",soup.text).groups()[0]
        check_immune(ability,pkmn)
    except:
        if not silent : print("No default set available")

def check_immune(ab,pkmn):
    
    if ab in ab_imm.keys():
        print("\n/!\\ According to its usual set", pkmn ,"is immune to",ab_imm[ab],"thanks to its ability",ab)
        print("It WILL be considered as a check for this type but will still appear as a weakness is the pokemon is vulnerable to it without the ability/!\\")
        checks[ab_imm[ab]]+=1


def print_stats(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    soup = soup.find('table',{'class': 'PokemonStats'})
    keys = soup.find_all('th')
    vals = soup.find_all('td')
    speed = soup.find('div',{'class': 'PokemonStats-speed-popup'})
    for i in range(len(keys)):
        print(keys[i].text,vals[i*2].text) #*2 because the bar that respresents the stat is also a td
    spdd = speed.find_all('td')    
    spdd[0] = 'Trick Room'
    spdd[2] = '0 EV'
    spdd[4] = 'Max EV'
    spdd[6] = 'Max EV + speed nature'
    print("_"*15,"SPEED DATA","_"*15)
    for i in range(4):#Trick Room // Default // Max neutral // Max nature
        print(spdd[2*i],":",spdd[2*i+1].text) #*2 because the bar that respresents the stat is also a td

def get_team(url,driver):
    driver.get(url)
    vtimeout = 3
    try:
        WebDriverWait(driver, vtimeout).until(EC.presence_of_element_located((By.CLASS_NAME,'chat battle-history')))
        print('page loaded !')
    except TimeoutException:
        print('Timeout exception encountered, the program will (probably) be fine')
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    soup = soup.find_all('div',{'class': 'chat battle-history'})
    datap1 = re.search(r"([\s\S]*)'s team: (.*)",soup[0].text).groups()
    datap2 = re.search(r"([\s\S]*)'s team: (.*)",soup[1].text).groups()
    p1 = datap1[0]
    p2 = datap2[0]
    team1 = datap1[1]
    team2 = datap2[1]
    team1 = team1.split(" / ")
    team2 = team2.split(" / ")
    return ({"name": p1,"team": team1},{"name": p2,"team": team2}) 

if __name__ == "__main__":
    try:
        (sys.argv[1] == None)
    except:    
        print("Please enter a url to scrap (after '-v' for verbose output)\nQuitting program")
        exit(0)
    
    match_url = sys.argv[1]

    if(sys.argv[1] == '-v'):
        silent=0
        match_url = sys.argv[2]
        
    print('Setting-up the driver please wait ...')
    opt = webdriver.ChromeOptions()
    opt.add_argument("--no-sandbox")
    opt.add_argument("--disable-dev-shm-usage")
    opt.add_argument("headless")
    opt.add_argument('log-level=3') #So Smogon doesn't tell me i'm deprecated :(
    opt.add_experimental_option('excludeSwitches',['enable-logging']) #Silence windowsdevtools is listenning message
    driver = webdriver.Chrome(executable_path=r'C:\Users\Alex\AppData\Local\Programs\Python\Python38-32\Lib\site-packages\chromedriver\chromedriver.exe',options=opt)
    print("Driver set-up !")
    print("Scrapping showdown")
    dataset = get_team(match_url,driver)

    driver.get('https://www.smogon.com/dex/ss/pokemon/foobar/')
    wrong = driver.page_source
    for player in dataset:
        print("\n\n\n","*"*15,player["name"],"*"*15)
        for pkmn in player["team"]:
            # if(pkmn == "-v"):
            #     continue
            if(not silent):
                print('\n\n','*'*15,'DATA FOR '+pkmn.upper(),"*"*15)
            url = 'https://www.smogon.com/dex/ss/pokemon/'+pkmn+'/'    
            driver.get(url)
            if driver.page_source == wrong:
                print("Page not found, skipping to the next pokemon")
                continue
            if(not silent):
                print('_'*15,'WEAKNESSES/RESISTANCES','_'*15)
            print_weakness(driver)
            if(not silent):
                print('_'*15,'USUAL SET','_'*15)
            print_usual_set(driver,pkmn)
            if(not silent):
                print('_'*15,'STATS DATA','_'*15)
                print_stats(driver)
        print('\n\n','*'*15,'TEAM\'S WEAKNESSES','*'*15)
        print_team_weak()
        reset_dicts()
    print('\n\nQuitting the driver now')
    driver.quit()
